## Documentación API de Registro de Usuarios en NestJS

### Introducción

Este documento describe las solicitudes (endpoints) disponibles en el microservicio de registro de usuarios de NestJS, los parámetros que reciben, su propósito y los roles de usuario necesarios para acceder a cada solicitud.

### DTOs (Data Transfer Objects)

Los DTOs definen la estructura de los datos que se envían y reciben en las solicitudes. A continuación, se detallan los DTOs utilizados en este microservicio:

* **RegisterUserDto:**
    * `location`: Una lista requerida de dos números que representan la ubicación (longitud y latitud).
* **RegisterUserByIdDto:**
    * `entry`: Una fecha y hora requerida para la entrada del usuario.
    * `exit`: Una fecha y hora requerida para la salida del usuario.
    * `entryLocation` (opcional): Una lista opcional de dos números que representan la ubicación de entrada (longitud y latitud).
    * `exitLocation` (opcional): Una lista opcional de dos números que representan la ubicación de salida (longitud y latitud).
* **EditRegisterDto** (opcional en algunas solicitudes):
    * `entry` (opcional): Una fecha y hora para actualizar la entrada del usuario.
    * `exit` (opcional): Una fecha y hora para actualizar la salida del usuario.
    * `entryLocation` (opcional): Una lista opcional de dos números para actualizar la ubicación de entrada (longitud y latitud).
    * `exitLocation` (opcional): Una lista opcional de dos números para actualizar la ubicación de salida (longitud y latitud).
* **HistoryDto** (opcional en algunas solicitudes):
    * `startDate` (opcional): Una fecha y hora para filtrar el historial por fecha de inicio.
    * `endDate` (opcional): Una fecha y hora para filtrar el historial por fecha de finalización.

### Guardias de autenticación

Este microservicio utiliza guardias para controlar el acceso a las solicitudes en función del rol del usuario:

* **AdminAuthGuard:** Solo permite el acceso a los administradores ("Admin").
* **UserAuthGuard:** Solo permite el acceso a los usuarios registrados ("User").

### Solicitudes

A continuación, se detalla cada solicitud (endpoint) del microservicio:

#### 1. Registrar un usuario

**Ruta:** `POST /register`

**Parámetros:**

* **Body:** Un objeto `RegisterUserDto` que contiene la ubicación del usuario.

**Descripción:** Registra un nuevo registro de entrada para el usuario autenticado actualmente.

**Rol necesario:** Usuario

#### 2. Registrar una salida

**Ruta:** `POST /register/exit`

**Parámetros:**

* **Body:** Un objeto `RegisterUserDto` que contiene la ubicación del usuario (opcional).

**Descripción:** Registra una nueva salida para el usuario autenticado actualmente.

**Rol necesario:** Usuario

#### 3. Registrar un usuario por ID

**Ruta:** `POST /register/:userId`

**Parámetros:**

* **Path:** `userId`: El ID numérico del usuario al que se registra la entrada/salida.
* **Body:** Un objeto `RegisterUserByIdDto` que contiene la fecha y hora de entrada, salida, y ubicaciones opcionales.

**Descripción:** Registra un nuevo registro de entrada y salida para un usuario específico, siempre que el usuario solicitante sea un administrador.

**Rol necesario:** Admin

#### 4. Editar un registro

**Ruta:** `PATCH /edit/:id`

**Parámetros:**

* **Path:** `id`: El ID numérico del registro que se va a editar.
* **Body:** Un objeto `EditRegisterDto` opcional que contiene las actualizaciones para la entrada, salida y ubicaciones (opcional).

**Descripción:** Edita un registro de entrada y/o salida existente, siempre que el usuario solicitante sea un administrador.

**Rol necesario:** Admin

#### 5. Obtener el historial del usuario actual

**Ruta:** `POST /history`

**Parámetros:**

* **Body:** Un objeto `HistoryDto` opcional para filtrar el historial por fechas.

**Descripción:** Obtiene el historial de registros de entrada y salida del usuario autenticado actualmente.

**Rol necesario:** Usuario

#### 6. Obtener el historial de un usuario por ID

**Ruta:** `POST /history/:userId`

**Parámetros:**

* **Path:** `userId`: El ID numérico del usuario del que se quiere obtener el historial.
* **Body:** Un objeto `HistoryDto` opcional para filtrar el historial por fechas.

**Descripción:** Obtiene el historial de registros de entrada y salida de un usuario específico

#### 7. Obtener el promedio de horas trabajadas por mes

**Ruta:** `GET /mean/:userId/:year`

**Parámetros:**

* **Path:**
    * `userId`: El ID numérico del usuario del que se quiere obtener el promedio.
    * `year`: El año numérico para el que se quiere obtener el promedio.

**Descripción:** Obtiene el promedio de horas trabajadas por mes del año especificado para el usuario especificado, siempre que el usuario solicitante sea un administrador.

**Rol necesario:** Admin
