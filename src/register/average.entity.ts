import { Entity, PrimaryGeneratedColumn, Column, Unique } from 'typeorm';
import { ArrayMaxSize, ArrayMinSize, IsNumber } from 'class-validator';

@Entity()
@Unique(['year', 'userId']) // Define the unique constraint
export class MonthlyWorkAverage {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ nullable: false })
  year: number;

  @Column({ name: 'user_id', type: 'integer', nullable: false })
  userId: number;

  @Column({ name: 'monthly_averages', type: 'json' })
  @ArrayMinSize(12) // Ensures at least 12 elements (all months)
  @ArrayMaxSize(12) // Restricts to a maximum of 12 elements
  @IsNumber({}, {each: true})
  monthlyAverages: number[];
}
