import {
  EntitySubscriberInterface,
  EventSubscriber,
  InsertEvent,
  UpdateEvent,
  RemoveEvent,
  Repository
} from 'typeorm';
import { Register } from './register.entity';
import { Between } from 'typeorm';
import { MonthlyWorkAverage } from './average.entity';

@EventSubscriber()
export class RegisterSubscriber implements EntitySubscriberInterface<Register> {
  listenTo() {
    return Register;
  }

  async beforeInsert(event: InsertEvent<Register>) {
    await this.validateEntryDate(event.manager.getRepository(Register), event.entity);
  }

  async beforeUpdate(event: UpdateEvent<Register>) {
    await this.validateEntryDate(event.manager.getRepository(Register), event.entity as Register);
  }

  async afterInsert(event: InsertEvent<Register>) {
    await this.updateMonthlyWorkAverage(event);
  }

  async afterUpdate(event: UpdateEvent<Register>) {
    await this.updateMonthlyWorkAverage(event);
  }

  async afterRemove(event: RemoveEvent<Register>) {
    await this.updateMonthlyWorkAverage(event);
  }

  private async validateEntryDate(repository: Repository<Register>, entity: Register) {
    const entryDate = entity.entry ? new Date(entity.entry) : new Date(); // Establecer entryDate a hoy si no está definido
    const startOfDay = new Date(entryDate.getFullYear(), entryDate.getMonth(), entryDate.getDate(), 0, 0, 0);
    const endOfDay = new Date(entryDate.getFullYear(), entryDate.getMonth(), entryDate.getDate(), 23, 59, 59);

    const existingEntry = await repository.findOne({
      where: {
        userId: entity.userId,
        entry: Between(startOfDay, endOfDay),
      },
    });

    if (existingEntry && existingEntry.id !== entity.id) {
      throw new Error(`An entry is already registered for user with ID ${entity.userId} on date ${entryDate.toDateString()}`);
    }
  }

  private async updateMonthlyWorkAverage(event: InsertEvent<Register> | UpdateEvent<Register> | RemoveEvent<Register>) {
    const entity = event.entity as Register;
    if (!entity || !entity.userId || !entity.entry) {
      return;
    }

    const year = new Date(entity.entry).getFullYear();
    const userId = entity.userId;

    const registers = await event.manager.find(Register, {
      where: {
        userId,
        entry: Between(new Date(`${year}-01-01`), new Date(`${year}-12-31`))
      }
    });

    const monthlyHours = Array(12).fill(0);
    const monthlyDays = Array(12).fill(0);

    registers.forEach(register => {
      const month = new Date(register.entry).getMonth();
      const day = new Date(register.entry).getDay();
      // Only consider Monday to Friday (1 to 5)
      if (day >= 1 && day <= 5 && register.workedHours) {
        monthlyHours[month] += register.workedHours;
        monthlyDays[month] += 1;
      }
    });

    const monthlyAverages = monthlyHours.map((totalHours, index) => {
      return monthlyDays[index] > 0 ? totalHours / monthlyDays[index] : 0;
    });

    let monthlyWorkAverage = await event.manager.findOne(MonthlyWorkAverage, { where: { year, userId } });

    if (!monthlyWorkAverage) {
      monthlyWorkAverage = new MonthlyWorkAverage();
      monthlyWorkAverage.year = year;
      monthlyWorkAverage.userId = userId;
    }

    monthlyWorkAverage.monthlyAverages = monthlyAverages;

    await event.manager.save(monthlyWorkAverage);
  }
}
