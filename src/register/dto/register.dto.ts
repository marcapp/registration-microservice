import {
  ArrayMaxSize,
  ArrayMinSize,
  IsNotEmpty,
  IsOptional,
  IsNumber,
  IsDateString
} from 'class-validator';

export class RegisterUserDto {
  @IsNotEmpty()
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsNumber({}, {each: true})
  location: number[]; // [longitude, latitude]
}

export class RegisterUserByIdDto {
  @IsNotEmpty()
  @IsDateString()
  entry: Date;

  @IsNotEmpty()
  @IsDateString()
  exit: Date;

  @IsOptional()
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsNumber({}, {each: true})
  entryLocation?: number[]; // [longitude, latitude]

  @IsOptional()
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsNumber({}, {each: true})
  exitLocation?: number[]; // [longitude, latitude]
}

export class EditRegisterDto {
  @IsOptional()
  @IsDateString()
  entry?: Date;

  @IsOptional()
  @IsDateString()
  exit?: Date;

  @IsOptional()
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsNumber({}, {each: true})
  entryLocation?: number[]; // [longitude, latitude]

  @IsOptional()
  @ArrayMinSize(2)
  @ArrayMaxSize(2)
  @IsNumber({}, {each: true})
  exitLocation?: number[]; // [longitude, latitude]
}

export class HistoryDto {
  @IsOptional()
  @IsDateString()
  startDate?: Date;

  @IsOptional()
  @IsDateString()
  endDate?: Date;
}
