import {
  Controller,
  Post,
  Patch,
  Get,
  Body,
  Param,
  Request,
  UseGuards,
  UsePipes,
  ValidationPipe
} from '@nestjs/common';
import { RegisterService } from './register.service';
import {
  RegisterUserDto,
  RegisterUserByIdDto,
  EditRegisterDto,
  HistoryDto
} from './dto';
import { AdminAuthGuard, UserAuthGuard } from './guards';

@Controller()
export class RegisterController {
  constructor(private readonly registerService: RegisterService) {}

  @UseGuards(UserAuthGuard)
  @Post('register')
  @UsePipes(new ValidationPipe({skipMissingProperties: true}))
  @UsePipes(new ValidationPipe({forbidNonWhitelisted: true, whitelist: true, transform: true}))
  async registerUser(@Request() request, @Body() registerUserDto: RegisterUserDto) {
    const userId = request.user.id;
    const authorRole = request.user.role;
    return this.registerService.registerUser(userId, registerUserDto, authorRole);
  }

  @UseGuards(UserAuthGuard)
  @Post('register/exit')
  @UsePipes(new ValidationPipe({skipMissingProperties: true}))
  @UsePipes(new ValidationPipe({forbidNonWhitelisted: true, whitelist: true, transform: true}))
  async registerUserExit(@Request() request, @Body() registerUserDto: RegisterUserDto) {
    const userId = request.user.id;
    return this.registerService.registerUserExit(userId, registerUserDto);
  }

  @UseGuards(AdminAuthGuard)
  @Post('register/:userId')
  @UsePipes(new ValidationPipe({skipMissingProperties: true}))
  @UsePipes(new ValidationPipe({forbidNonWhitelisted: true, whitelist: true, transform: true}))
  async registerUserById(@Request() request, @Param('userId') userId: number, @Body() registerUserByIdDto: RegisterUserByIdDto) {
    const authorRole = request.user.role;
    return this.registerService.registerUserById(userId, registerUserByIdDto, authorRole);
  }

  @UseGuards(AdminAuthGuard)
  @Patch('edit/:id')
  @UsePipes(new ValidationPipe({skipMissingProperties: true}))
  @UsePipes(new ValidationPipe({forbidNonWhitelisted: true, whitelist: true, transform: true}))
  async editRegister(@Request() request, @Param('id') id: number, @Body() editRegisterDto: EditRegisterDto) {
    const authorRole = request.user.role;
    return this.registerService.editRegister(id, editRegisterDto, authorRole);
  }

  @UseGuards(UserAuthGuard)
  @Post('history')
  @UsePipes(new ValidationPipe({skipMissingProperties: true}))
  @UsePipes(new ValidationPipe({forbidNonWhitelisted: true, whitelist: true, transform: true}))
  async getUserHistory(@Request() request, @Body() historyDto: HistoryDto) {
    const userId = request.user.id;
    return this.registerService.getUserRegisters(userId, historyDto);
  }

  @UseGuards(AdminAuthGuard)
  @Post('history/:userId')
  @UsePipes(new ValidationPipe({skipMissingProperties: true}))
  @UsePipes(new ValidationPipe({forbidNonWhitelisted: true, whitelist: true, transform: true}))
  async getUserHistoryById(@Param('userId') userId: number, @Body() historyDto: HistoryDto) {
    return this.registerService.getUserRegisters(userId, historyDto);
  }

  @UseGuards(AdminAuthGuard)
  @Get('mean/:userId/:year')
  @UsePipes(new ValidationPipe({transform: true}))
  async getUserMonthlyMean(
    @Param('userId') userId: number,
    @Param('year') year: number
  ) {
    return this.registerService.getUserMonthlyMean(userId, year);
  }
}
