import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
  BeforeUpdate,
  Point
} from 'typeorm';

@Entity({ name: 'register' })
export class Register {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ name: 'user_id', type: 'integer', nullable: false })
  userId: number;

  @Column({ type: 'timestamp with time zone', nullable: false })
  entry: Date;

  @Column({ type: 'timestamp with time zone', nullable: true })
  exit: Date;

  @Column({
    name: 'worked_hours', type: 'numeric', nullable: true,
    // https://github.com/typeorm/typeorm/issues/873#issuecomment-502294597
    transformer: {
      to(data?: number | null): number | null {
        return data ?? null;
      },
      from(data?: string | null): number | null {
        if (data === null || data === undefined) {
          return null;
        }

        const parsedValue = parseFloat(data);
        return isNaN(parsedValue) ? null : parsedValue;
      }
    }
  })
  workedHours: number;

  @Column({ type: 'integer', nullable: true })
  day: number;

  @Column({ name: 'author_role', type: 'varchar', nullable: false })
  authorRole: string;

  @Column({ name: 'entry_location', type: 'geometry', spatialFeatureType: 'Point', srid: 4326, nullable: true })
  entryLocation: Point;

  @Column({ name: 'exit_location', type: 'geometry', spatialFeatureType: 'Point', srid: 4326, nullable: true })
  exitLocation: Point;

  @BeforeInsert()
  @BeforeUpdate()
  async calculateWorkedHoursAndDay() {
    if (this.entry && this.exit) {
      const entryDate = new Date(this.entry);
      const exitDate = new Date(this.exit);

      if (exitDate > entryDate) {
        this.workedHours = Math.abs((exitDate.getTime() - entryDate.getTime()) / (1000 * 60 * 60)); // Diferencia en horas
        this.day = entryDate.getDay(); // Número del día de la semana (0 para domingo, 6 para sábado)
      } else {
        throw new Error('The exit date must be after the entry date');
      }
    }
  }
}
