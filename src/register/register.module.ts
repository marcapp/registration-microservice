import { Module } from '@nestjs/common';
import { HttpModule } from '@nestjs/axios';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RegisterController } from './register.controller';
import { RegisterService } from './register.service';
import { RegisterSubscriber } from './register.subscriber';
import { Register } from './register.entity';
import { MonthlyWorkAverage } from './average.entity';
import { UserAuthGuard } from './guards/user-auth.guard';

@Module({
  imports: [
    HttpModule,
    TypeOrmModule.forFeature([Register, MonthlyWorkAverage]),
  ],
  controllers: [RegisterController],
  providers: [RegisterService, RegisterSubscriber, UserAuthGuard],
})
export class RegisterModule {}
