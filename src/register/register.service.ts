import {
  Injectable,
  ConflictException,
  NotFoundException,
  BadRequestException
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, Between } from 'typeorm';
import { Register } from './register.entity';
import { MonthlyWorkAverage } from './average.entity';
import {
  RegisterUserDto,
  RegisterUserByIdDto,
  EditRegisterDto,
  HistoryDto
} from './dto/register.dto';

@Injectable()
export class RegisterService {
  constructor(
    @InjectRepository(Register)
    private readonly registerRepository: Repository<Register>,
    @InjectRepository(MonthlyWorkAverage)
    private readonly monthlyWorkAverageRepository: Repository<MonthlyWorkAverage>,
  ) {}

  async registerUser(userId: number, registerUserDto: RegisterUserDto, authorRole: string): Promise<Register> {
    const { location } = registerUserDto;
    const today = new Date();

    const newRegister = this.registerRepository.create({
      userId,
      entry: today,
      entryLocation: { type: 'Point', coordinates: location },
      authorRole,
    });

    try {
      return await this.registerRepository.save(newRegister);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async registerUserById(userId: number, registerUserByIdDto: RegisterUserByIdDto, authorRole: string): Promise<Register> {
    const { entry, exit, entryLocation, exitLocation } = registerUserByIdDto;
    const newRegister = this.registerRepository.create({
      userId,
      entry: new Date(entry),
      exit: new Date(exit),
      entryLocation: entryLocation ? { type: 'Point', coordinates: entryLocation } : null,
      exitLocation: exitLocation ? { type: 'Point', coordinates: exitLocation } : null,
      authorRole,
    });

    try {
      return await this.registerRepository.save(newRegister);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async registerUserExit(userId: number, registerUserDto: RegisterUserDto): Promise<Register> {
    const { location } = registerUserDto;
    const today = new Date();
    const startOfDay = new Date(today.setHours(0, 0, 0, 0));
    const endOfDay = new Date(today.setHours(23, 59, 59, 999));

    const register = await this.registerRepository.findOne({
      where: {
        userId,
        entry: Between(startOfDay, endOfDay),
      },
    });

    if (!register) {
      throw new NotFoundException('No entry found for today.');
    } else if (register.exit) {
      throw new ConflictException('Exit already registered for today.');
    }

    register.exit = new Date();
    register.exitLocation = { type: 'Point', coordinates: location };

    try {
      return await this.registerRepository.save(register);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async editRegister(id: number, editRegisterDto: EditRegisterDto, authorRole: string): Promise<Register> {
    const register = await this.registerRepository.findOneBy({ id });
    if (!register) {
      throw new NotFoundException('Register not found');
    }

    Object.assign(register, {
      ...editRegisterDto,
      entryLocation: editRegisterDto.entryLocation ? { type: 'Point', coordinates: editRegisterDto.entryLocation } : register.entryLocation,
      exitLocation: editRegisterDto.exitLocation ? { type: 'Point', coordinates: editRegisterDto.exitLocation } : register.exitLocation,
      authorRole,
    });

    try {
      return await this.registerRepository.save(register);
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async getUserRegisters(userId: number, historyDto: HistoryDto): Promise<Register[]> {
    const { startDate, endDate } = historyDto;

    const query = this.registerRepository.createQueryBuilder('register')
      .where('register.userId = :userId', { userId });

    if (startDate) {
      query.andWhere('register.entry >= :startDate', { startDate });
    }

    if (endDate) {
      query.andWhere('register.entry <= :endDate', { endDate });
    }

    try {
      return await query.orderBy('register.entry', 'ASC').getMany();
    } catch (error) {
      throw new BadRequestException(error.message);
    }
  }

  async getUserMonthlyMean(userId: number, year: number): Promise<number[]> {
    const monthlyWorkAverage = await this.monthlyWorkAverageRepository.findOne({
      where: {
        userId,
        year,
      },
    });

    if (!monthlyWorkAverage) {
      throw new NotFoundException(`Monthly work averages not found for user with ID ${userId} in year ${year}`);
    }

    return monthlyWorkAverage.monthlyAverages;
  }
}
