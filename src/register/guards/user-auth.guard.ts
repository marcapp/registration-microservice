import { CanActivate, ExecutionContext, Injectable, UnauthorizedException } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { ConfigService } from '@nestjs/config';
import { firstValueFrom } from 'rxjs';

@Injectable()
export class UserAuthGuard implements CanActivate {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService
  ) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest();
    const authHeader = request.headers['authorization'];

    if (!authHeader) {
      throw new UnauthorizedException('No authorization header found');
    }

    const usersMicroserviceUrl = this.configService.get<string>('USERS_MICROSERVICE_URL');
    let userData;

    try {
      const response = await firstValueFrom(this.httpService.get(`${usersMicroserviceUrl}`, {
        headers: { authorization: authHeader },
      }));
      userData = response.data;
    } catch (err) {
      const errorMessage = err.response?.data?.message || 'Token validation failed, could not fetch user data';
      throw new UnauthorizedException(errorMessage);
    }

    // Check if userData contains the expected keywords
    if (!userData || typeof userData !== 'object' || !('id' in userData) || !('role' in userData)) {
      throw new UnauthorizedException('Token validation failed, invalid user data received from microservice');
    }
    request.user = userData;
    return true;
  }
}
