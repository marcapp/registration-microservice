import { Injectable, ExecutionContext, UnauthorizedException } from '@nestjs/common';
import { UserAuthGuard } from './user-auth.guard';

@Injectable()
export class AdminAuthGuard extends UserAuthGuard {
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isAuthenticated = await super.canActivate(context);

    if (!isAuthenticated) {
      return false;
    }

    const request = context.switchToHttp().getRequest();
    const user = request.user;

    if (!user) {
      throw new UnauthorizedException('User data not found');
    }

    if (user.role !== 'Admin') {
      throw new UnauthorizedException('User is not an admin');
    }

    return true;
  }
}
