CREATE TABLE "register" (
    "id" SERIAL PRIMARY KEY,
    "user_id" integer NOT NULL,
    "entry" TIMESTAMP WITH TIME ZONE NOT NULL,
    "exit" TIMESTAMP WITH TIME ZONE,
    "worked_hours" numeric,
    "day" integer,
    "author_role" character varying NOT NULL,
    "entry_location" geometry(Point,4326),
    "exit_location" geometry(Point,4326)
);

CREATE TABLE "monthly_work_average" (
    "id" SERIAL PRIMARY KEY,
    "year" integer NOT NULL,
    "user_id" integer NOT NULL,
    "monthly_averages" json NOT NULL,
    CONSTRAINT "UQ_a1176b84f29114a0d61ef58c970" UNIQUE ("year", "user_id")
);
