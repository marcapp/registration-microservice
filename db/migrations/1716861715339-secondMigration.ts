import { MigrationInterface, QueryRunner } from "typeorm";

export class SecondMigration1716861715339 implements MigrationInterface {
    name = 'SecondMigration1716861715339'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TYPE "public"."register_movement_enum" AS ENUM('Entrance', 'Exit')`);
        await queryRunner.query(`ALTER TABLE "register" ADD "movement" "public"."register_movement_enum" NOT NULL`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`ALTER TABLE "register" DROP COLUMN "movement"`);
        await queryRunner.query(`DROP TYPE "public"."register_movement_enum"`);
    }

}
