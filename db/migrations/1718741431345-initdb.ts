import { MigrationInterface, QueryRunner } from "typeorm";

export class Initdb1718741431345 implements MigrationInterface {
    name = 'Initdb1718741431345'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "register" ("id" SERIAL NOT NULL, "user_id" integer NOT NULL, "entry" TIMESTAMP WITH TIME ZONE NOT NULL, "exit" TIMESTAMP WITH TIME ZONE, "worked_hours" numeric, "day" integer, "author_role" character varying NOT NULL, "entry_location" geometry(Point,4326), "exit_location" geometry(Point,4326), CONSTRAINT "PK_14473cc8f2caa81fd19f7648d54" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "monthly_work_average" ("id" SERIAL NOT NULL, "year" integer NOT NULL, "user_id" integer NOT NULL, "monthly_averages" json NOT NULL, CONSTRAINT "UQ_a1176b84f29114a0d61ef58c970" UNIQUE ("year", "user_id"), CONSTRAINT "PK_6a9075a73b2b5874d29d3fd52f8" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "monthly_work_average"`);
        await queryRunner.query(`DROP TABLE "register"`);
    }

}
